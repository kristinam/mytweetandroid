package com.example.cexwaterford.tweet.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.widget.EditText;

import com.example.cexwaterford.tweet.R;


public  class MyPreferences extends FragmentActivity{

    private EditText username;
    private EditText password;
    private EditText update_interval;


        @Override
        public void onCreate( Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_preferences);
            getActionBar().setDisplayHomeAsUpEnabled(true);

        }

        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
            username.setText(sharedPrefs.getString("username", "NOUSERNAME"));
            password.setText(sharedPrefs.getString("password", "NOPASSWORD"));
            update_interval.setText(sharedPrefs.getString("update_interval", "NOUPDATE"));
        }


    }

