package com.example.cexwaterford.tweet.models;

import android.util.Log;

import java.util.ArrayList;

import static com.example.cexwaterford.android.helpers.LogHelpers.info;

public class Portfolio
{
    public  ArrayList<Tweet>  tweets;
    private PortfolioSerializer   serializer;

    public Portfolio(PortfolioSerializer serializer)
    {
        //this.generateTestData();
        this.serializer = serializer;
        try
        {
            tweets = serializer.loadTweets();
        }
        catch (Exception e)
        {
            info(this, "Error loading residences: " + e.getMessage());
            tweets = new ArrayList<Tweet>();
        }
    }

    public boolean saveTweets()
    {
        try
        {
            serializer.saveTweets(tweets);
            info(this, "Tweets saved to file");
            return true;
        }
        catch (Exception e)
        {
            info(this, "Error saving tweets: " + e.getMessage());
            return false;
        }
    }

    public void addTweet(Tweet tweet)
    {

        tweets.add(tweet);
    }

    public Tweet getTweet(String id)
    {
        Log.i(this.getClass().getSimpleName(), "String parameter id: " + id);

        for (Tweet tw : tweets)
        {
            if(id.equals(tw.id))
            {
                return tw;
            }
        }
        info(this, "failed to find tweet. returning first element array to avoid crash");
        return null;
    }

    public void deleteTweet(Tweet tweet)
    {
        tweets.remove(tweet);
    }

    public void clearList()
    {
        tweets.removeAll(tweets);
    }

    /**
    private void generateTestData()
    {
        for(int i = 0; i < 3; i += 1)
        {
            Tweet tweet = new Tweet();
            tweet.message_text = "hello";

            tweets.add(tweet);
        }
    }*/

}
