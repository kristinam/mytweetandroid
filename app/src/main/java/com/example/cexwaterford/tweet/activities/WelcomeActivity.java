package com.example.cexwaterford.tweet.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.cexwaterford.tweet.R;
import com.example.cexwaterford.tweet.app.TweetApp;
import com.example.cexwaterford.tweet.models.Tweeter;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Cex Waterford on 19/12/2015.
 */
public class WelcomeActivity extends Activity implements Callback<List<Tweeter>>
{
    private TweetApp app;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        app = (TweetApp) getApplication();

    }


    @Override
    public void onResume()
    {
        super.onResume();
        Call<List<Tweeter>> call = app.tweetService.getAllTweeters();
        call.enqueue(this);

    }

    @Override
    public void onResponse(Response<List<Tweeter>> response, Retrofit retrofit)
    {
        serviceAvailableMessage();
        app.tweeters = response.body();
        app.tweetServiceAvailable = true;

    }

    @Override
    public void onFailure(Throwable t)
    {
        app.tweetServiceAvailable = false;
        serviceUnavailableMessage();

    }

    public void loginPressed (View view)
    {
        if (app.tweetServiceAvailable)
        {
            startActivity (new Intent(this, Login.class));
        }
        else
        {
            serviceUnavailableMessage();
        }
    }

    public void signupPressed (View view)
    {
        if (app.tweetServiceAvailable)
        {
            startActivity (new Intent(this, Signup.class));
        }
        else
        {
            serviceUnavailableMessage();
        }
    }

    void serviceUnavailableMessage()
    {
        //Toast toast = Toast.makeText(this, "Donation Service Unavailable. Try again later", Toast.LENGTH_LONG);
        Toast toast = Toast.makeText(this, "Failed to retrieve tweeter list", Toast.LENGTH_LONG);
        toast.show();
    }

    void serviceAvailableMessage()
    {
        int numberTweeters = app.tweeters.size();
        Toast toast = Toast.makeText(this, "Retrieved " + numberTweeters + " tweeters", Toast.LENGTH_LONG);
        toast.show();
    }
}


