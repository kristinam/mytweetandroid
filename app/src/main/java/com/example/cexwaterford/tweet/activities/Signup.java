package com.example.cexwaterford.tweet.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cexwaterford.tweet.R;
import com.example.cexwaterford.tweet.app.TweetApp;
import com.example.cexwaterford.tweet.models.Tweeter;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Cex Waterford on 19/12/2015.
 */
public class Signup extends Activity implements Callback<Tweeter>
{
    private TweetApp app;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        app = (TweetApp) getApplication();
    }

    public void registerPressed (View view)
    {
        TextView firstName = (TextView)  findViewById(R.id.firstName);
        TextView lastName  = (TextView)  findViewById(R.id.lastName);
        TextView email     = (TextView)  findViewById(R.id.Email);
        TextView password  = (TextView)  findViewById(R.id.Password);

       Tweeter tweeter = new Tweeter(firstName.getText().toString(), lastName.getText().toString(), email.getText().toString(), password.getText().toString());

        TweetApp app = (TweetApp) getApplication();
        Call<Tweeter> call = (Call<Tweeter>) app.tweetService.createTweeter(tweeter);
        call.enqueue(this);

    }


    @Override
    public void onResponse(Response<Tweeter> response, Retrofit retrofit) {
        app.tweeters.add(response.body());
        startActivity(new Intent(this, WelcomeActivity.class));

    }

    @Override
    public void onFailure(Throwable t)
    {
        app.tweetServiceAvailable = false;
        Toast toast = Toast.makeText(this, "Tweet Service Unavailable. Try again later", Toast.LENGTH_LONG);
        toast.show();
        startActivity (new Intent(this, WelcomeActivity.class));
    }
}