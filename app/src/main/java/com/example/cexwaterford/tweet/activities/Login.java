package com.example.cexwaterford.tweet.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cexwaterford.tweet.R;
import com.example.cexwaterford.tweet.app.TweetApp;

/**
 * Created by Cex Waterford on 19/12/2015.
 */
public class Login extends Activity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void signinPressed (View view)
    {
        TweetApp app = (TweetApp) getApplication();

        TextView email     = (TextView)  findViewById(R.id.loginEmail);
        TextView password  = (TextView)  findViewById(R.id.loginPassword);

        if (app.validTweeter(email.getText().toString(), password.getText().toString()))
        {
            startActivity (new Intent(this, TweetListActivity.class));
        }
        else
        {
            Toast toast = Toast.makeText(this, "Invalid Credentials", Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}