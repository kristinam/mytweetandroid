package com.example.cexwaterford.tweet.models;

import java.util.UUID;

/**
 * The id is required immediately on the client-side
 * We send to id the server in the http call and configure the server to allow this practice
 */

public class Tweeter
{
    public String id;
    public String firstName;
    public String lastName;
    public String email;
    public String password;


    public Tweeter(String firstName, String lastName, String email, String password)
    {
        this.id = UUID.randomUUID().toString();
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }



}