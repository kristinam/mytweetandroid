package com.example.cexwaterford.tweet.activities;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import com.example.cexwaterford.tweet.R;

import com.example.cexwaterford.tweet.app.TweetApp;
import com.example.cexwaterford.tweet.models.Portfolio;
import com.example.cexwaterford.tweet.models.Tweet;

import java.util.ArrayList;

import static com.example.cexwaterford.android.helpers.LogHelpers.info;

public class TweetPagerActivity extends FragmentActivity  implements ViewPager.OnPageChangeListener

{
    private ViewPager viewPager;
    private ArrayList<Tweet> tweets;
    private Portfolio portfolio;
    private PagerAdapter pagerAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        viewPager = new ViewPager(this);
        viewPager.setId(R.id.viewPager);
        setContentView(viewPager);
        setTweetList();

        pagerAdapter = new PagerAdapter(getSupportFragmentManager(), tweets);
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(this);

        setCurrentItem();
    }

    private void setTweetList()
    {
        TweetApp app = (TweetApp) getApplication();
        portfolio = app.portfolio;
        tweets = portfolio.tweets;
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
    {
        info(this, "onPageScrolled: position "+position+" positionOffset "+positionOffset+" positionOffsetPixels "+positionOffsetPixels);
        Tweet tweet = tweets.get(position);
        setTitle("Mytweet");
        /**if (tweet.message_text != null)
        {
            setTitle(tweet.message_text);
        }*/
    }


    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void setCurrentItem()
    {
        String tw = (String)getIntent().getSerializableExtra(TweetFragment.EXTRA_TWEET_ID);
        for (int i = 0; i < tweets.size(); i++)
        {
            if (tweets.get(i).id.toString().equals(tw.toString()))
            {
                viewPager.setCurrentItem(i);
                break;
            }
        }
    }


    class PagerAdapter extends FragmentStatePagerAdapter
    {
        private ArrayList<Tweet>  tweets;

        public PagerAdapter(FragmentManager fm, ArrayList<Tweet> tweets)
        {
            super(fm);
            this.tweets = tweets;
        }

        @Override
        public int getCount()
        {
            return tweets.size();
        }

        @Override
        public Fragment getItem(int pos)
        {
            Tweet tweet= tweets.get(pos);
            Bundle args = new Bundle();
            args.putSerializable(TweetFragment.EXTRA_TWEET_ID, tweet.id);
            TweetFragment fragment = new TweetFragment();
            fragment.setArguments(args);
            return fragment;
        }
    }
}

