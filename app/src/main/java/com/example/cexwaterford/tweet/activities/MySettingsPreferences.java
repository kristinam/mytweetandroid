package com.example.cexwaterford.tweet.activities;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.example.cexwaterford.tweet.R;

/**
 * Created by Cex Waterford on 01/11/2015.
 */
public class MySettingsPreferences extends PreferenceFragment{

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}
