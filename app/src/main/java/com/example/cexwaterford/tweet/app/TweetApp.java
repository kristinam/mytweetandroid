package com.example.cexwaterford.tweet.app;

import android.app.Application;

import com.example.cexwaterford.tweet.main.TweetServiceProxy;
import com.example.cexwaterford.tweet.models.Portfolio;
import com.example.cexwaterford.tweet.models.PortfolioSerializer;
import com.example.cexwaterford.tweet.models.Tweet;
import com.example.cexwaterford.tweet.models.Tweeter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

import static com.example.cexwaterford.android.helpers.LogHelpers.info;


public class TweetApp extends Application
{
    private static final String FILENAME = "portfolio.json";
    public Portfolio portfolio;

    //public String            service_url  = "http://10.0.2.2:9000";    //Android Emulator
    //public String          service_url  = "http://10.0.3.2:9000";    //Genymotion
   // public String          service_url  = "https://donation-service-2015.herokuapp.com/";
    public String          service_url  = "http://mytweetbykristina.herokuapp.com/";
    public TweetServiceProxy tweetService;
    public boolean tweetServiceAvailable = false;

    public Tweeter loggedInTweeter;
    public List<Tweeter> tweeters = new ArrayList<Tweeter>();
    public List<Tweet> tweets = new ArrayList<Tweet>();

    public void newTweet(Tweet tweet)
    {
        tweets.add(tweet);
    }

    public void newTweeter(Tweeter tweeter)
    {
        tweeters.add(tweeter);
    }

    public boolean validTweeter (String email, String password)
    {
        for (Tweeter tweeter : tweeters)
        {
            if (tweeter.email.equals(email) && tweeter.password.equals(password))
            {
                loggedInTweeter = tweeter;
                return true;
            }
        }
        return false;
    }



    @Override
    public void onCreate()
    {
        super.onCreate();
        PortfolioSerializer serializer = new PortfolioSerializer(this, FILENAME);
        portfolio = new Portfolio(serializer);

        Gson gson = new GsonBuilder().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(service_url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        tweetService = retrofit.create(TweetServiceProxy.class);

        info(this, "MyTweet app launched");
    }

}
